import { TurnContext } from 'botbuilder';
import { QnAMakerEndpoint, QnAMakerOptions } from 'botbuilder-ai';
/**
 * A simple bot that responds to utterances with answers from QnA Maker.
 * If an answer is not found for an utterance, the bot responds with help.
 */
export declare class QnAMakerBot {
    private qnaMaker;
    /**
     * The QnAMakerBot constructor requires one argument (`endpoint`) which is used to create an instance of `QnAMaker`.
     * @param endpoint The basic configuration needed to call QnA Maker. In this sample the configuration is retrieved from the .bot file.
     * @param config An optional parameter that contains additional settings for configuring a `QnAMaker` when calling the service.
     */
    constructor(endpoint: QnAMakerEndpoint, qnaOptions?: QnAMakerOptions);
    /**
     * Every conversation turn for our QnA Bot will call this method.
     * There are no dialogs used, since it's "single turn" processing, meaning a single request and
     * response, with no stateful conversation.
     * @param turnContext A TurnContext instance, containing all the data needed for processing the conversation turn.
     */
    onTurn(turnContext: TurnContext): Promise<void>;
}

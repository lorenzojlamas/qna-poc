"use strict";
// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const botbuilder_1 = require("botbuilder");
const botbuilder_ai_1 = require("botbuilder-ai");
/**
 * A simple bot that responds to utterances with answers from QnA Maker.
 * If an answer is not found for an utterance, the bot responds with help.
 */
class QnAMakerBot {
    /**
     * The QnAMakerBot constructor requires one argument (`endpoint`) which is used to create an instance of `QnAMaker`.
     * @param endpoint The basic configuration needed to call QnA Maker. In this sample the configuration is retrieved from the .bot file.
     * @param config An optional parameter that contains additional settings for configuring a `QnAMaker` when calling the service.
     */
    constructor(endpoint, qnaOptions) {
        this.qnaMaker = new botbuilder_ai_1.QnAMaker(endpoint, qnaOptions);
    }
    /**
     * Every conversation turn for our QnA Bot will call this method.
     * There are no dialogs used, since it's "single turn" processing, meaning a single request and
     * response, with no stateful conversation.
     * @param turnContext A TurnContext instance, containing all the data needed for processing the conversation turn.
     */
    onTurn(turnContext) {
        return __awaiter(this, void 0, void 0, function* () {
            // By checking the incoming Activity type, the bot only calls QnA Maker in appropriate cases.
            if (turnContext.activity.type === botbuilder_1.ActivityTypes.Message) {
                // Perform a call to the QnA Maker service to retrieve matching Question and Answer pairs.
                const qnaResults = yield this.qnaMaker.generateAnswer(turnContext.activity.text);
                // If an answer was received from QnA Maker, send the answer back to the user.
                if (qnaResults[0]) {
                    yield turnContext.sendActivity(qnaResults[0].answer);
                    // If no answers were returned from QnA Maker, reply with help.
                }
                else {
                    yield turnContext.sendActivity('No QnA Maker answers were found. This example uses a QnA Maker Knowledge Base that focuses on smart light bulbs. To see QnA Maker in action, ask the bot questions like "Why won\'t it turn on?" or say something like "I need help."');
                }
                // If the Activity is a ConversationUpdate, send a greeting message to the user.
            }
            else if (turnContext.activity.type === botbuilder_1.ActivityTypes.ConversationUpdate &&
                turnContext.activity.recipient.id !== turnContext.activity.membersAdded[0].id) {
                yield turnContext.sendActivity('Welcome to the QnA Maker sample! Ask me a question and I will try to answer it.');
                // Respond to all other Activity types.
            }
            else if (turnContext.activity.type !== botbuilder_1.ActivityTypes.ConversationUpdate) {
                yield turnContext.sendActivity(`[${turnContext.activity.type}]-type activity detected.`);
            }
        });
    }
}
exports.QnAMakerBot = QnAMakerBot;
//# sourceMappingURL=bot.js.map
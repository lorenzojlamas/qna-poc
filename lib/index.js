"use strict";
// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = require("dotenv");
const path = require("path");
const restify = require("restify");
const botbuilder_1 = require("botbuilder");
const bot_1 = require("./bot");
// Read botFilePath and botFileSecret from .env file.
// Note: Ensure you have a .env file and include botFilePath and botFileSecret.
const ENV_FILE = path.join(__dirname, '..', '.env');
const loadFromEnv = dotenv_1.config({ path: ENV_FILE });
// Get the .bot file path.
// See https://aka.ms/about-bot-file to learn more about .bot file its use and bot configuration.
const BOT_FILE = path.join(__dirname, '..', (process.env.botFilePath || ''));
// let botConfig: BotConfiguration;
// try {
//     // read bot configuration from .bot file.
//     botConfig = BotConfiguration.loadSync(BOT_FILE, process.env.botFileSecret);
// } catch (err) {
//     console.error(`\nError reading bot file. Please ensure you have valid botFilePath and botFileSecret set for your environment.`);
//     console.error(`\n - The botFileSecret is available under appsettings for your Azure Bot Service bot.`);
//     console.error(`\n - If you are running this bot locally, consider adding a .env file with botFilePath and botFileSecret.`);
//     console.error(`\n - See https://aka.ms/about-bot-file to learn more about .bot file its use and bot configuration.\n\n`);
//     process.exit();
// }
// For local development configuration as defined in .bot file.
const DEV_ENVIRONMENT = 'development';
// Bot name as defined in .bot file or from runtime.
// See https://aka.ms/about-bot-file to learn more about .bot files.
// const BOT_CONFIGURATION = (process.env.NODE_ENV || DEV_ENVIRONMENT);
// const QNA_CONFIGURATION = 'QnABot';
// Get bot endpoint and QnAMaker configuration by service name.
// const endpointConfig = botConfig.findServiceByNameOrId(BOT_CONFIGURATION) as IEndpointService;
// const qnaConfig = botConfig.findServiceByNameOrId(QNA_CONFIGURATION) as IQnAService;
// Map the contents to the required format for QnAMaker.
const qnaEndpointSettings = {
    endpointKey: '07002d32-36c3-416a-bc90-d54b643d1c03',
    host: 'https://legpocqna.azurewebsites.net/qnamaker',
    knowledgeBaseId: 'a5458f2b-b120-4160-867f-92bacbe37f40',
};
// Create adapter. See https://aka.ms/about-bot-adapter to learn more about adapters.
const adapter = new botbuilder_1.BotFrameworkAdapter({
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword,
});
// Catch-all for errors.
adapter.onTurnError = (context, error) => __awaiter(this, void 0, void 0, function* () {
    console.error(`\n [onTurnError]: ${error}`);
    yield context.sendActivity(`Oops. Something went wrong!`);
});
// Create the QnAMakerBot.
let bot;
try {
    bot = new bot_1.QnAMakerBot(qnaEndpointSettings, {});
}
catch (err) {
    console.error(`[botInitializationError]: ${err}`);
    process.exit();
}
// Create HTTP server
const server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, () => {
    console.log(`\n${server.name} listening to ${server.url}`);
    console.log(`\nGet Bot Framework Emulator: https://aka.ms/botframework-emulator`);
    console.log(`\nTo talk to your bot, open qnamaker.bot file in the emulator.`);
});
// Listen for incoming requests.
server.post('/api/messages', (req, res) => __awaiter(this, void 0, void 0, function* () {
    yield adapter.processActivity(req, res, (turnContext) => __awaiter(this, void 0, void 0, function* () {
        yield bot.onTurn(turnContext);
    }));
}));
//# sourceMappingURL=index.js.map